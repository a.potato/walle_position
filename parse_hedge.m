%% interpolate hedge positions to 10 Hz frequency
freq = 10;
tmin = min([h1.ts; h2.ts]);
tmin = tmin - mod(tmin, 1/freq);
tmax = max([h1.ts; h2.ts]);
tmax = tmax - mod(tmax, 1/freq) + 1/freq;
tq = (tmin:1/freq:tmax)';

h1_x = interp1(h1.ts, h1.x, tq, 'pchip', 'extrap');
h1_y = interp1(h1.ts, h1.y, tq, 'pchip', 'extrap');
h2_x = interp1(h2.ts, h2.x, tq, 'pchip', 'extrap');
h2_y = interp1(h2.ts, h2.y, tq, 'pchip', 'extrap');

figure
hold on
plot(h1_x, h1_y, '.-');
plot(h2_x, h2_y, '.-');
axis equal

%% prediction

freq = 1; % ������� ������� ���������
buf_len = 5; % ����� ������ ��� �������������, � ��������
t_add = 0.1; % ��������� �� ������ �������

tmin = max([h1.ts(1) h2.ts(1)]);
tmin = tmin - mod(tmin, 1/freq);
tmax = max([h1.ts; h2.ts]);
tmax = tmax - mod(tmax, 1/freq) + 1/freq;

extrap.t = tmin + 5.0:1/freq:tmax;
extrap_count = length(extrap.t);
extrap.h1.x = zeros(extrap_count, 1);
extrap.h1.y = zeros(extrap_count, 1);
extrap.h2.x = zeros(extrap_count, 1);
extrap.h2.y = zeros(extrap_count, 1);

for i = 1:extrap_count
    %hedge 1 filtered by time
    t = extrap.t(i);
    hf1.filter = h1.ts < t & h1.ts > t - buf_len;
    hf1.ts = h1.ts(hf1.filter);
    hf1.x = h1.x(hf1.filter);
    hf1.y = h1.y(hf1.filter);
    
    hf2.filter = h2.ts < t & h2.ts > t - buf_len;
    hf2.ts = h1.ts(hf2.filter);
    hf2.x = h1.x(hf2.filter);
    hf2.y = h1.y(hf2.filter);
    
    h1_interp_x = interp1(hf1.ts, hf1.x, [hf1.ts; t], 'cubic', 'extrap');
    h1_interp_y = interp1(hf1.ts, hf1.y, [hf1.ts; t], 'cubic', 'extrap');
    h2_interp_x = interp1(hf2.ts, hf2.x, [hf2.ts; t], 'cubic', 'extrap');
    h2_interp_y = interp1(hf2.ts, hf2.y, [hf2.ts; t], 'cubic', 'extrap');
    
    extrap.h1.x(i) = h1_interp_x(length(h1_interp_x));
    extrap.h1.y(i) = h1_interp_y(length(h1_interp_y));
    extrap.h2.x(i) = h2_interp_x(length(h2_interp_x));
    extrap.h2.y(i) = h2_interp_y(length(h2_interp_y));    
end

%%
h1_x = interp1(h1.ts, h1.x, tq, 'cubic', 'extrap');
h1_y = interp1(h1.ts, h1.y, tq, 'cubic', 'extrap');
h2_x = interp1(h2.ts, h2.x, tq, 'cubic', 'extrap');
h2_y = interp1(h2.ts, h2.y, tq, 'cubic', 'extrap');

%% speed
v1_x = (h1.x(2:h1_count) - h1.x(1:h1_count-1)) ./ (h1.ts(2:h1_count) - h1.ts(1:h1_count-1));
v1_y = (h1.y(2:h1_count) - h1.y(1:h1_count-1)) ./ (h1.ts(2:h1_count) - h1.ts(1:h1_count-1));
v1 = sqrt(v1_x.^2 + v1_y.^2);

v2_x = (h2.x(2:h2_count) - h2.x(1:h2_count-1)) ./ (h2.ts(2:h2_count) - h2.ts(1:h2_count-1));
v2_y = (h2.y(2:h2_count) - h2.y(1:h2_count-1)) ./ (h2.ts(2:h2_count) - h2.ts(1:h2_count-1));
v2 = sqrt(v2_x.^2 + v2_y.^2);

hold on
plot(h1.ts(2:h1_count), v1)
plot(h2.ts(2:h2_count), v2)

%% time check
hold on
%plot(h1.ts(2:length(h1.ts)), h1.time(2:length(h1.ts)) / 1000000000 - h1.ts(2:length(h1.ts)));
%plot(h2.ts(2:length(h2.ts)), h2.time(2:length(h2.ts)) / 1000000000 - h2.ts(2:length(h2.ts)));

plot(h1.ts(2:length(h1.ts)), h1.ts(2:length(h1.ts)) - h1.ts(1:length(h1.ts)-1));
plot(h2.ts(2:length(h2.ts)), h2.ts(2:length(h2.ts)) - h2.ts(1:length(h2.ts)-1));

%%
yyaxis left
plot(xsens_vel_ts, xsens_vel_twistz)
yyaxis right
plot(cmd_ts, cmd_wangle)

%%

plotyy(xsens_vel_ts, xsens_vel_twistz, cmd_ts, cmd_wangle)

%%
last_addr = fieldhedge_posaddress(1);
for i = 2:length(fieldhedge_posaddress)
    if fieldhedge_posaddress(i) == last_addr
        fprintf('problem at %d\n', i);
    end
    last_addr = fieldhedge_posaddress(i);
end

%plot(fieldhedge_posx_m, fieldhedge_posy_m, '.')
