%%
clear;
%filename = 'C:\Users\Hyper Station\Documents\MATLAB\walle_position\iner\indoori.csv';
filename = 'C:\Users\Hyper Station\Documents\MATLAB\walle_position\iner\indoor_05_10_2017_1.csv';
run('import_raw_hedge');

%%
clear h;
addr = unique(fieldhedge_posaddress);
for i = 1:length(addr)
    count = sum(fieldhedge_posaddress(:) == addr(i));

    condition = fieldhedge_posaddress == addr(i);
    fieldheadertime = time / 1e9;
    h(i, 1) = extract(condition, time, fieldheadertime, fieldhedge_posx_m, fieldhedge_posy_m, fieldhedge_posz_m);
end
%%
min_time_array = zeros(length(h), 1);
for i = 1:length(h)
    min_time_array(i) = min([h(i).ts(1), h(i).time(1) / 1e9]);
end
min_time = min(min_time_array);
for i = 1:length(h)
    h(i).ts = h(i).ts - min_time;
    h(i).time = h(i).time - min_time * 1e9;
end

%%
hold on
axis equal
grid
for i = 1:length(h)
    plot(h(i).x, h(i).y, '.');
    %plot(mean(h(i).x), mean(h(i).y), '.', 'MarkerSize', 40);
end

%%
figure
plot(h1.ts, h1.x, h2.ts, h2.x);
figure
plot(h1.ts, h1.y, h2.ts, h2.y);

%%
fprintf('h1.x=%d; h2.x=%d; h1.y=%d; h2.y=%d\n', std(h1.x), std(h2.x), std(h1.y), std(h2.y));

%%
extrap_hedge = prediction(h1,h2);
[extrap_pos.x, extrap_pos.y, extrap_pos.yaw] = pos_from_hedges(extrap_hedge.h1, extrap_hedge.h2);
extrap_pos.t = extrap_hedge.t;

ext_filter = ~isnan(extrap_hedge.h1.x);
ext_pos_filt.x = extrap_pos.x(ext_filter);
ext_pos_filt.y = extrap_pos.y(ext_filter);
ext_pos_filt.yaw = extrap_pos.yaw(ext_filter);
ext_pos_filt.t = extrap_pos.t(ext_filter);

%%
raw = get_raw_pos(h1,h2);

%%

figure
hold on
axis equal
plot(raw.x, raw.y, '.-');
plot(extrap_pos.x, extrap_pos.y, '.-');
plot(h1.x, h1.y);
plot(h2.x, h2.y);

figure
hold on
plot(extrap_pos.t, unwrap(extrap_pos.yaw));
plot(raw.t, unwrap(raw.yaw));