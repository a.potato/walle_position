%%
clear;
run('import_hal_hedge');

%%
h1_count = sum(fieldhedge_posaddress(:) == 4);
h2_count = sum(fieldhedge_posaddress(:) == 6);

h1_condition = fieldhedge_posaddress == 4;
h1 = extract(h1_condition, time, fieldheadertime, fieldhedge_posx_m, fieldhedge_posy_m, fieldhedge_posz_m);
h2 = extract(~h1_condition, time, fieldheadertime, fieldhedge_posx_m, fieldhedge_posy_m, fieldhedge_posz_m);

min_time = min([h1.ts(1), h2.ts(1), h1.time(1) / 1e9, h2.time(1) / 1e9]);
h1.ts = h1.ts - min_time;
h2.ts = h2.ts - min_time;
h1.time = h1.time - min_time * 1e9;
h1.time = h1.time - min_time * 1e9;

%hold on
%plot(h1.x, h1.y, '.-');
%plot(h2.x, h2.y, '.-');

%%
extrap_hedge = prediction(h1,h2);
[extrap_pos.x, extrap_pos.y, extrap_pos.yaw] = pos_from_hedges(extrap_hedge.h1, extrap_hedge.h2);
extrap_pos.t = extrap_hedge.t;

ext_filter = ~isnan(extrap_hedge.h1.x);
ext_pos_filt.x = extrap_pos.x(ext_filter);
ext_pos_filt.y = extrap_pos.y(ext_filter);
ext_pos_filt.yaw = extrap_pos.yaw(ext_filter);
ext_pos_filt.t = extrap_pos.t(ext_filter);

%%
raw = get_raw_pos(h1,h2);

%%

figure
hold on
axis equal
plot(raw.x, raw.y, '.-');
plot(extrap_pos.x, extrap_pos.y, '.-');
plot(h1.x, h1.y);
plot(h2.x, h2.y);

figure
hold on
plot(extrap_pos.t, unwrap(extrap_pos.yaw));
plot(raw.t, unwrap(raw.yaw));