function h = convert_to_hedge(addr, posaddress, time, headertime, posx, posy, posz)

count = sum(posaddress(:) == addr);

condition = posaddress == addr;
h = extract(condition, time, headertime, posx, posy, posz);
end