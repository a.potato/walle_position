function [extrap] = prediction(h1, h2)

freq = 10; % ������� ������� ���������
buf_len = 3; % ����� ������ ��� �������������, � ��������
MIN_DATA_COUNT = 2; % ����������� ���������� ��������� � ������� ��� �������������
t_add = 0; % ��������� ��������, � ��������
WITH_PLOT = 0;

% ����������� ������� ��������� ������� - �� ����������� �� ������������
% ����� � ����������� �� �������
tmin = max([h1.ts(1) h2.ts(1)]);
tmin = tmin - mod(tmin, 1/freq);
tmax = max([h1.ts; h2.ts]);
tmax = tmax - mod(tmax, 1/freq) + 1/freq;

% allocate arrays
extrap.t = tmin:1/freq:tmax;
extrap_count = length(extrap.t);
extrap.h1.x = NaN(extrap_count, 1);
extrap.h1.y = NaN(extrap_count, 1);
extrap.h2.x = NaN(extrap_count, 1);
extrap.h2.y = NaN(extrap_count, 1);

if WITH_PLOT
    figure_handle = figure;
    hold on
    axis equal
    h1_real_handle = plot(h1.x, h1.y, 'b');
    h2_real_handle = plot(h2.x, h2.y, 'b');
    h1_ext_handle = plot(h1.x(1), h1.y(1));
    h2_ext_handle = plot(h2.x(1), h2.y(1));
    h1_path_handle = plot(h1.x(1), h1.y(1));
    h2_path_handle = plot(h2.x(1), h2.y(1));
end

for i = 1:extrap_count
    %hedge 1 filtered by time
    t = extrap.t(i);
    
    % ������ ������, ���������� �� ��������� ��������
    hf1.filter = h1.ts <= t & h1.ts > t - buf_len;
    hf2.filter = h2.ts <= t & h2.ts > t - buf_len;
    
    hf1.ts = h1.ts(hf1.filter);
    hf1.x = h1.x(hf1.filter);
    hf1.y = h1.y(hf1.filter);
    
    hf2.ts = h2.ts(hf2.filter);
    hf2.x = h2.x(hf2.filter);
    hf2.y = h2.y(hf2.filter);
    
    if sum(hf1.filter) < MIN_DATA_COUNT || sum(hf2.filter) < MIN_DATA_COUNT
        fprintf('not enough points, skipping: %f\n', t);
        continue;
    end
    
    h1_interp_x = interp1(hf1.ts, hf1.x, [hf1.ts; t + t_add], 'pchip', 'extrap');
    h1_interp_y = interp1(hf1.ts, hf1.y, [hf1.ts; t + t_add], 'pchip', 'extrap');
    h2_interp_x = interp1(hf2.ts, hf2.x, [hf2.ts; t + t_add], 'pchip', 'extrap');
    h2_interp_y = interp1(hf2.ts, hf2.y, [hf2.ts; t + t_add], 'pchip', 'extrap');
    
    extrap.h1.x(i) = h1_interp_x(length(h1_interp_x));
    extrap.h1.y(i) = h1_interp_y(length(h1_interp_y));
    extrap.h2.x(i) = h2_interp_x(length(h2_interp_x));
    extrap.h2.y(i) = h2_interp_y(length(h2_interp_y)); 
    
    if WITH_PLOT
        delete(h1_ext_handle);
        delete(h2_ext_handle);

        h1_ext_handle = plot(h1_interp_x, h1_interp_y, '.-r',...
             'LineWidth',2,...
             'MarkerSize', 15);
        h2_ext_handle = plot(h2_interp_x, h2_interp_y, '.-r',...
             'LineWidth',2,...
             'MarkerSize', 15);   

        delete(h1_path_handle);
        delete(h2_path_handle);
        h1_path_handle = plot(extrap.h1.x, extrap.h1.y, 'g');
        h2_path_handle = plot(extrap.h2.x, extrap.h2.y, 'g');

        pause(0.1);
        if ~ishandle(figure_handle)
            break
        end
    end
end

end