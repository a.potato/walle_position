%% Import data from text file.
% Script for importing data from the following text file:
%
%    C:\Users\Hyper Station\Documents\MATLAB\walle_position\iner\hedge_pos.csv
%
% To extend the code to different selected data or a different text file,
% generate a function instead of a script.

% Auto-generated by MATLAB on 2017/09/19 21:11:17

%% Initialize variables.
filename = 'C:\Users\Hyper Station\Documents\MATLAB\walle_position\iner\hedge_pos.csv';
delimiter = ',';
startRow = 2;

%% Format string for each line of text:
%   column1: double (%f)
%	column2: double (%f)
%   column8: double (%f)
%	column10: double (%f)
%   column11: double (%f)
%	column12: double (%f)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%f%f%*s%*s%*s%*s%*s%f%*s%f%f%f%*s%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to format string.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);

%% Close the text file.
fclose(fileID);

%% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

%% Allocate imported array to column variable names
time = dataArray{:, 1};
fieldheadertime = dataArray{:, 2};
fieldhedge_posaddress = dataArray{:, 3};
fieldhedge_posx_m = dataArray{:, 4};
fieldhedge_posy_m = dataArray{:, 5};
fieldhedge_posz_m = dataArray{:, 6};


%% Clear temporary variables
clearvars filename delimiter startRow formatSpec fileID dataArray ans;