function [ raw ] = get_raw_pos( h1, h2 )
%GET_RAW_POS Summary of this function goes here
%   Detailed explanation goes here

%% create unified dataset with raw h1/h2 positions
clear uni
uni.t = unique([h1.ts; h2.ts]);
len = length(uni.t);

% preallocate arrays
uni.h1.x = zeros(len,1);
uni.h1.y = zeros(len,1);
uni.h2.x = zeros(len,1);
uni.h2.y = zeros(len,1);

filter1 = ismember(uni.t, h1.ts);
uni.h1.x(filter1) = h1.x;
uni.h1.y(filter1) = h1.y;

filter2 = ismember(uni.t, h2.ts);
uni.h2.x(filter2) = h2.x;
uni.h2.y(filter2) = h2.y;

% ��������� ������ ����� ����������� ����������
for i = 2:length(uni.t)
    if uni.h1.x(i) == 0 && uni.h1.y(i) == 0
        uni.h1.x(i) = uni.h1.x(i-1);
        uni.h1.y(i) = uni.h1.y(i-1);
    end
    if uni.h2.x(i) == 0 && uni.h2.y(i) == 0
        uni.h2.x(i) = uni.h2.x(i-1);
        uni.h2.y(i) = uni.h2.y(i-1);
    end
end

% ������ �����, ����� ��� ���� ������ �� ����� �������
start_index = max([find(uni.h1.x, 1) find(uni.h2.x, 1)]);
uni_trim.t = uni.t(start_index:len);
uni_trim.h1.x = uni.h1.x(start_index:len);
uni_trim.h1.y = uni.h1.y(start_index:len);
uni_trim.h2.x = uni.h2.x(start_index:len);
uni_trim.h2.y = uni.h2.y(start_index:len);

[raw.x, raw.y, raw.yaw] = pos_from_hedges(uni_trim.h1, uni_trim.h2);
raw.t = uni_trim.t;

end

