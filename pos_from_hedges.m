function [ x0, y0, yaw ] = pos_from_hedges( h1, h2 )
%GET_TRUCK_POS Summary of this function goes here
%   Detailed explanation goes here

% hedge positions on robot base
ax2 = 0;
ay2 = 6.6;
bx2 = 0;
by2 = -6.6;

ax1_ = h1.x;
ay1_ = h1.y;
bx1_ = h2.x;
by1_ = h2.y;

ab_raw_length = sqrt((ax1_ - bx1_) .^ 2 + (ay1_ - by1_) .^ 2);
ab_real_length = sqrt((ax2 - bx2) .^ 2 + (ay2 - by2) .^ 2);
k = ab_real_length ./ ab_raw_length;

ax1 = 0.5 * (ax1_ .* (1 + k) + bx1_ .* (1 - k));
ay1 = 0.5 * (ay1_ .* (1 + k) + by1_ .* (1 - k));

bx1 = 0.5 * (ax1_ .* (1 - k) + bx1_ .* (1 + k));
by1 = 0.5 * (ay1_ .* (1 - k) + by1_ .* (1 + k));

a_x = bx1 - ax1;
a_y = by1 - ay1;
b_x = bx2 - ax2;
b_y = by2 - ay2;

yaw = atan2(a_y * b_x - a_x * b_y, a_x * b_x + a_y * b_y);
yaw_cos = cos(yaw);
yaw_sin = sin(yaw);

x0 = bx1 - bx2 * yaw_cos + by2 * yaw_sin;
y0 = by1 - bx2 * yaw_sin - by2 * yaw_cos;
yaw = yaw + pi;

% figure
% hold on
% plot(h1_x, h1_y, '.-');
% plot(h2_x, h2_y, '.-');
% plot(x0, y0, '.-');
% axis equal
% 
% figure
% hold on
% plot(tq, yaw, '.-');

end

